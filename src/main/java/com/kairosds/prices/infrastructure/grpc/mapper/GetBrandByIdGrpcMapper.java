package com.kairosds.prices.infrastructure.grpc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.kairosds.prices.application.dto.BrandDto;
import com.kairosds.prices.infrastructure.grpc.GetBrandByIdRequest;
import com.kairosds.prices.infrastructure.grpc.GetBrandByIdResponse;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GetBrandByIdGrpcMapper {

	BrandDto toDto(GetBrandByIdRequest getBrandByIdRequest);

	GetBrandByIdResponse toResponse(BrandDto brandDto);

}
