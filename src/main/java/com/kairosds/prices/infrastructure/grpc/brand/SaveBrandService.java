package com.kairosds.prices.infrastructure.grpc.brand;

import com.kairosds.prices.application.dto.BrandDto;
import com.kairosds.prices.application.usecases.brand.SaveBrandUseCase;
import com.kairosds.prices.infrastructure.grpc.SaveBrandRequest;
import com.kairosds.prices.infrastructure.grpc.SaveBrandResponse;
import com.kairosds.prices.infrastructure.grpc.SaveBrandServiceGrpc.SaveBrandServiceImplBase;
import com.kairosds.prices.infrastructure.grpc.mapper.SaveBrandGrpcMapper;

import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;

@RequiredArgsConstructor
@GrpcService
public class SaveBrandService extends SaveBrandServiceImplBase {

	private final SaveBrandUseCase saveBrandUseCase;

	private final SaveBrandGrpcMapper saveBrandGrpcMapper;

	@Override
	public void saveBrand(SaveBrandRequest request, StreamObserver<SaveBrandResponse> responseObserver) {
		BrandDto brandDto = saveBrandUseCase.execute(saveBrandGrpcMapper.toDto(request));
		responseObserver.onNext(saveBrandGrpcMapper.toResponse(brandDto));
		responseObserver.onCompleted();
	}

}
