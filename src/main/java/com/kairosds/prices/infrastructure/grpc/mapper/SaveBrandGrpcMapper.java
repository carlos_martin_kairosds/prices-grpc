package com.kairosds.prices.infrastructure.grpc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.kairosds.prices.application.dto.BrandDto;
import com.kairosds.prices.infrastructure.grpc.SaveBrandRequest;
import com.kairosds.prices.infrastructure.grpc.SaveBrandResponse;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SaveBrandGrpcMapper {

	BrandDto toDto(SaveBrandRequest saveBrandRequest);

	SaveBrandResponse toResponse(BrandDto brandDto);

}
