package com.kairosds.prices.infrastructure.grpc.brand;

import java.util.NoSuchElementException;

import com.kairosds.prices.application.dto.BrandDto;
import com.kairosds.prices.application.usecases.brand.GetBrandByIdUseCase;
import com.kairosds.prices.infrastructure.grpc.GetBrandByIdRequest;
import com.kairosds.prices.infrastructure.grpc.GetBrandByIdResponse;
import com.kairosds.prices.infrastructure.grpc.GetBrandByIdServiceGrpc.GetBrandByIdServiceImplBase;
import com.kairosds.prices.infrastructure.grpc.mapper.GetBrandByIdGrpcMapper;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;

@RequiredArgsConstructor
@GrpcService
public class GetBrandByIdService extends GetBrandByIdServiceImplBase {

	private final GetBrandByIdUseCase getBrandByIdUseCase;

	private final GetBrandByIdGrpcMapper getBrandByIdGrpcMapper;

	@Override
	public void getBrandById(GetBrandByIdRequest request, StreamObserver<GetBrandByIdResponse> responseObserver) {
		try {
			BrandDto brandDto = getBrandByIdUseCase.execute(request.getId());
			responseObserver.onNext(getBrandByIdGrpcMapper.toResponse(brandDto));
			responseObserver.onCompleted();
		} catch (NoSuchElementException e) {
			responseObserver.onError(Status.NOT_FOUND.withDescription("Brand not found").asException());
		}
	}

}
