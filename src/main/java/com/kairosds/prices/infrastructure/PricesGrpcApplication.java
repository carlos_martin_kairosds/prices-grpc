package com.kairosds.prices.infrastructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricesGrpcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricesGrpcApplication.class, args);
	}

}
